package com.calendartemplate;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.datamedic.boyfrienddiary.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import java.io.IOException;
import java.util.Calendar;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends AppCompatActivity {





    private static final String TAG = "MainActivity";

    private AdView mAdView;
    private InterstitialAd mInterstitialAd;


    Button Boyfriend, SaveCb, FrontListB, Visibility, Visibility2, Settings;
    CalendarView calendarView;
    public static String BfFocus;
    public boolean Flag = false;
    public boolean Flag2 = false;

    public static String BfHeiPoun;
    public static String BfId;
    public static Uri data2;
    TextView BfText;
    ImageView BfMapa;
    public static String FbString = " ";
    public static String PhString = " ";
    public static String InstString = " ";
    public static String DtString = " ";
    public static String KsString = " ";
    public static String MoString = " ";
    public static String SxString = " ";
    public static String JeString = " ";


    public static String CanString = " ";
    public static String NagString = " ";
    public static String IndString = " ";
    public static String CheaString = " ";
    public static String NtalString = " ";
    public static String BreaString = " ";

    public String dateCalendar;

    DataBase dataBase;
    ProgressBar progressBar;
    TextView barText;
    private int progressTime = 0;
    private Handler mHandler = new Handler();

    public long calendarIntl;
    String fbCheck = "FaceBook";
    String phCheck = "Phone";
    String insCheck = "Instagram";
    int PERMISSION_REQUEST_CODE = 100;
    ResizeAndRotate resizeAndRotate;
    CheckBox Fb, Ph, Inst, Dt, Ks, Mo, Sx, Je, Can, Nag, Ind, Chea, Ntal, Break;
    EditText Com;
    Bitmap bitmap;

    private AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.5F);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!checkPermission()) {
            openActivity();
        } else {
            if (checkPermission()) {
                requestPermissionAndContinue();
            } else {
                openActivity();
            }
        }


        setContentView(R.layout.activity_main);
        resizeAndRotate = new ResizeAndRotate();



        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.intersitialID));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        SharedPreferences pref = getSharedPreferences("MyPrefs", MODE_PRIVATE);
        setBfId(pref.getString("BfId", null));
        BfFocus = pref.getString("BfNL", "No Boyfriend Selected");
        setBfHeiPoun(pref.getString("BfHP", "  "));
        setData2(Uri.parse(pref.getString("BfIm", "android.resource://com.datamedic.boyfrienddiary/drawable/emptyface")));

        Boyfriend = findViewById(R.id.BfListButton);
        calendarView = findViewById(R.id.calendarView);
        BfText = findViewById(R.id.BfText);
        FrontListB = findViewById(R.id.FrontListButton);
        BfMapa = findViewById(R.id.BfMapa);
        Settings = findViewById(R.id.setting);
        dataBase = new DataBase(this);
        BfText.setText(BfFocus);


        bitmap = BitmapFactory.decodeFile(resizeAndRotate.getPath(this, getData2()));


        String path = "";
        path = resizeAndRotate.getPath(this, getData2());
        if (path != null ) {
            try {
                getResizedBitmap(bitmap, 20, 20);
                BfMapa.setImageBitmap(resizeAndRotate.modifyOrientation(bitmap, resizeAndRotate.getPath(this, getData2())));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else BfMapa.setImageURI(getData2());


        Settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(buttonClick);
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), Setting.class);
                startActivity(intent);

            }
        });


        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                setDateCalendar(dayOfMonth + "/" + (month + 1) + "/" + year);

                Calendar cal = Calendar.getInstance();
                cal.set(year, month, dayOfMonth);

                calendarIntl = cal.getTimeInMillis();


                AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);
                final View mView = getLayoutInflater().inflate(R.layout.checkbox, null);
                Fb = mView.findViewById(R.id.Fb);
                Ph = mView.findViewById(R.id.Ph);
                Inst = mView.findViewById(R.id.Inst);
                Dt = mView.findViewById(R.id.Dt);
                Ks = mView.findViewById(R.id.Ks);
                Mo = mView.findViewById(R.id.Mo);
                Sx = mView.findViewById(R.id.Sx);
                Com = mView.findViewById(R.id.Comment);
                Je = mView.findViewById(R.id.Fight);
                Can = mView.findViewById(R.id.Cancel);
                Nag = mView.findViewById(R.id.insult);
                Ind = mView.findViewById(R.id.disablephone);
                Chea = mView.findViewById(R.id.Cheat);
                Ntal = mView.findViewById(R.id.Reject);
                Break = mView.findViewById(R.id.Break);
                Com = mView.findViewById(R.id.Comment);
                SaveCb = mView.findViewById(R.id.BtSaveCb);
                ImageView faceImg = mView.findViewById(R.id.FacebookIMG);
                ImageView PhoImg = mView.findViewById(R.id.PhoneIMG);
                ImageView InstImg = mView.findViewById(R.id.InstagramIMG);


                final ConstraintLayout constraintLayout = mView.findViewById(R.id.Expandable);
                constraintLayout.setVisibility(mView.GONE);

                final ConstraintLayout constraintLayout2 = mView.findViewById(R.id.Expandable2);
                constraintLayout2.setVisibility(mView.GONE);


                Visibility = mView.findViewById(R.id.Viss);
                Visibility2 = mView.findViewById(R.id.Viss2);

                Visibility.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (!Flag) {
                            constraintLayout2.setVisibility(mView.GONE);
                            constraintLayout.setVisibility(mView.VISIBLE);

                            Flag = true;
                            Flag2 = false;
                        } else {
                            constraintLayout.setVisibility(mView.GONE);
                            Flag = false;
                        }


                    }

                });


                Visibility2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (!Flag2) {
                            constraintLayout2.setVisibility(mView.VISIBLE);
                            constraintLayout.setVisibility(mView.GONE);
                            Flag2 = true;
                            Flag = false;
                        } else {
                            constraintLayout2.setVisibility(mView.GONE);
                            Flag2 = false;
                        }


                    }

                });

//
                if (dataBase.chFace(getBfId())) {
                    Fb.setEnabled(true);
                } else {
                    Fb.setEnabled(false);
                    faceImg.setImageURI(Uri.parse("android.resource://com.calendartemplate/drawable/facebookgrey"));
                }
                Fb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                        if (isChecked) {
                            setFbString(getString(R.string.FB));

                            Toast.makeText(getApplicationContext(), FbString + " : is Checked", Toast.LENGTH_SHORT).show();
                        } else setFbString(getString(R.string.Emt));

                    }
                });

                if (dataBase.chPhone(getBfId())) {
                    Ph.setEnabled(true);
                } else {
                    Ph.setEnabled(false);
                    PhoImg.setImageURI(Uri.parse("android.resource://com.calendartemplate/drawable/phonegrey"));
                }
                Ph.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            setPhString(getString(R.string.Phone));

                            Toast.makeText(getApplicationContext(), PhString + " : is Checked", Toast.LENGTH_SHORT).show();
                        } else setPhString(getString(R.string.Emt));
                    }
                });

                if (dataBase.chInsta(getBfId())) {
                    Inst.setEnabled(true);
                } else {
                    Inst.setEnabled(false);
                    InstImg.setImageURI(Uri.parse("android.resource://com.calendartemplate/drawable/instagramgrey"));
                }
                Inst.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            setInstString(getString(R.string.Ins));

                            Toast.makeText(getApplicationContext(), InstString + " : is Checked", Toast.LENGTH_SHORT).show();
                        } else setInstString(getString(R.string.Emt));
                    }
                });

                Dt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            setDtString(getString(R.string.Date));

                            Toast.makeText(getApplicationContext(), DtString + " : is Checked", Toast.LENGTH_SHORT).show();
                        } else setDtString(getString(R.string.Emt));
                    }
                });

                Ks.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            setKsString(getString(R.string.Kiss));

                            Toast.makeText(getApplicationContext(), KsString + " : is Checked", Toast.LENGTH_SHORT).show();
                        } else setKsString(getString(R.string.Emt));
                    }
                });

                Mo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            setMoString(getString(R.string.Makeout));

                            Toast.makeText(getApplicationContext(), MoString + " : is Checked", Toast.LENGTH_SHORT).show();
                        } else setMoString(getString(R.string.Emt));
                    }
                });

                Sx.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            setSxString(getString(R.string.Sex));

                            Toast.makeText(getApplicationContext(), SxString + " : is Checked", Toast.LENGTH_SHORT).show();
                        } else setSxString(getString(R.string.Emt));
                    }
                });


                Je.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            setJeString(getString(R.string.Fight));

                            Toast.makeText(getApplicationContext(), JeString + " : is Checked", Toast.LENGTH_SHORT).show();
                        } else setJeString(getString(R.string.Emt));
                    }
                });

                Can.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            setCanString(getString(R.string.Cancel));

                            Toast.makeText(getApplicationContext(), CanString + " : is Checked", Toast.LENGTH_SHORT).show();
                        } else setCanString(getString(R.string.Emt));
                    }
                });

                Nag.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            setNagString(getString(R.string.insult));

                            Toast.makeText(getApplicationContext(), NagString + " : is Checked", Toast.LENGTH_SHORT).show();
                        } else setNagString(getString(R.string.Emt));
                    }
                });


                Ind.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            setIndString(getString(R.string.disablephone));

                            Toast.makeText(getApplicationContext(), IndString + " : is Checked", Toast.LENGTH_SHORT).show();
                        } else setIndString(getString(R.string.Emt));
                    }
                });

                Chea.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            setCheaString(getString(R.string.Cheating));

                            Toast.makeText(getApplicationContext(), CheaString + " : is Checked", Toast.LENGTH_SHORT).show();
                        } else setCheaString(getString(R.string.Emt));
                    }
                });

                Ntal.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            setNtalString(getString(R.string.Reject));

                            Toast.makeText(getApplicationContext(), NtalString + " : is Checked", Toast.LENGTH_SHORT).show();
                        } else setNtalString(getString(R.string.Emt));
                    }
                });
                Break.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                        if (isChecked) {
                            setBreaString(getString(R.string.Breaking));

                            Toast.makeText(getApplicationContext(), BreaString + " : is Checked", Toast.LENGTH_SHORT).show();
                        } else setBreaString(getString(R.string.Emt));

                    }
                });

                mBuilder.setView(mView);
                final AlertDialog dialog = mBuilder.create();

                if (getBfId() == null) {
                    Toast.makeText(getApplicationContext(), "No Boyfriend is Checked", Toast.LENGTH_SHORT).show();
                } else {
                    dialog.show();
                }

                SaveCb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        boolean inserted = dataBase.data2(getDateCalendar(), getFbString(), getPhString(), getInstString(), getDtString(), getKsString(), getMoString(), getSxString(), Com.getText().toString(), getBfId()
                                , getJeString(), getCanString(), getNagString(), getIndString(), getCheaString(), getNtalString(), getBreaString(), calendarIntl, fbCheck, phCheck, insCheck);

                        if (inserted) {
                            Toast.makeText(MainActivity.this, "Inserted", Toast.LENGTH_SHORT).show();

                            if (mInterstitialAd.isLoaded()) {
                                mInterstitialAd.show();
                            } else {
                                Log.d("TAG", "The interstitial wasn't loaded yet.");
                            }

                            dialog.cancel();
                            Intent i = new Intent(MainActivity.this, MainActivity.class);

                            startActivity(i);
                            finish();
                            onRestart();



                            setFbString(getString(R.string.Emt));
                            setPhString(getString(R.string.Emt));
                            setInstString(getString(R.string.Emt));
                            setDtString(getString(R.string.Emt));
                            setKsString(getString(R.string.Emt));
                            setMoString(getString(R.string.Emt));
                            setSxString(getString(R.string.Emt));
                            setJeString(getString(R.string.Emt));
                            setCanString(getString(R.string.Emt));
                            setNagString(getString(R.string.Emt));
                            setIndString(getString(R.string.Emt));
                            setCheaString(getString(R.string.Emt));
                            setNtalString(getString(R.string.Emt));
                            setBreaString(getString(R.string.Emt));

                        } else
                            Toast.makeText(MainActivity.this, " Not Inserted", Toast.LENGTH_SHORT).show();


                    }
                });

            }

        });


        Boyfriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(buttonClick);
                AlertDialog.Builder mBuilder2 = new AlertDialog.Builder(MainActivity.this);
                final View mView = getLayoutInflater().inflate(R.layout.progress, null);

                progressBar = mView.findViewById(R.id.progress);
                barText = mView.findViewById(R.id.bartext);
                mBuilder2.setView(mView);
                final AlertDialog dialog = mBuilder2.create();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (progressTime < 100) {
                            progressTime++;
                            android.os.SystemClock.sleep(50);
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    progressBar.setProgress(progressTime);
                                    if (progressTime == 70) {
                                        Intent intent = new Intent();
                                        intent.setClass(getApplicationContext(), BoyfriendList.class);
                                        startActivity(intent);


                                    }
                                }
                            });
                        }
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                barText.setVisibility(View.VISIBLE);
                                android.os.SystemClock.sleep(100);
                                dialog.cancel();
                                finish();

                            }
                        });
                    }
                }).start();


                dialog.show();


            }
        });

        FrontListB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(buttonClick);
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), FrontList.class);
                startActivity(intent);


                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    Log.d("TAG", "The interstitial wasn't loaded yet.");
                }



                finish();



            }
        });



        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.

            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when when the interstitial ad is closed.
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
                Log.d("TAG2", "ok.");
            }
        });




    }

    private boolean checkPermission() {

        return ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                ;
    }

    private void requestPermissionAndContinue() {
        if (ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, WRITE_EXTERNAL_STORAGE)
                    && ActivityCompat.shouldShowRequestPermissionRationale(this, READ_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{WRITE_EXTERNAL_STORAGE,
                        READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
            } else {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{WRITE_EXTERNAL_STORAGE,
                        READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
            }
        } else {
            openActivity();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (permissions.length > 0 && grantResults.length > 0) {

                boolean flag = true;
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        flag = false;
                    }
                }
                if (flag) {
                    openActivity();
                } else {
                    finish();
                }

            } else {
                finish();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void openActivity() {
        //add your further process after giving permission or to download images from remote server.
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;

        // create a matrix for the manipulation
        Matrix matrix = new Matrix();

        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);

        // recreate the new Bitmap
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);

        return resizedBitmap;
    }

    public String getDateCalendar() {
        return dateCalendar;
    }

    public void setDateCalendar(String dateCalendar1) {
        dateCalendar = dateCalendar1;
    }

    public static String getFbString() {
        return FbString;
    }

    public static void setFbString(String fbString) {
        FbString = fbString;
    }

    public static String getPhString() {
        return PhString;
    }

    public static void setPhString(String phString) {
        PhString = phString;
    }

    public static String getInstString() {
        return InstString;
    }

    public static void setInstString(String instString) {
        InstString = instString;
    }

    public static String getDtString() {
        return DtString;
    }

    public static void setDtString(String dtString) {
        DtString = dtString;
    }

    public static String getKsString() {
        return KsString;
    }

    public static void setKsString(String ksString) {
        KsString = ksString;
    }

    public static String getMoString() {
        return MoString;
    }

    public static void setMoString(String moString) {
        MoString = moString;
    }

    public static String getSxString() {
        return SxString;
    }

    public static void setSxString(String sxString) {
        SxString = sxString;
    }

    public static String getBfId() {
        return BfId;
    }

    public static void setBfId(String bdId) {
        BfId = bdId;
    }

    public static String getBfHeiPoun() {
        return BfHeiPoun;
    }

    public static String getJeString() {
        return JeString;
    }

    public static void setJeString(String jeString) {
        JeString = jeString;
    }

    public static String getCanString() {
        return CanString;
    }

    public static void setCanString(String canString) {
        CanString = canString;
    }

    public static String getNagString() {
        return NagString;
    }

    public static void setNagString(String nagString) {
        NagString = nagString;
    }

    public static String getIndString() {
        return IndString;
    }

    public static void setIndString(String indString) {
        IndString = indString;
    }

    public static String getCheaString() {
        return CheaString;
    }

    public static void setCheaString(String cheaString) {
        CheaString = cheaString;
    }

    public static String getNtalString() {
        return NtalString;
    }

    public static void setNtalString(String ntalString) {
        NtalString = ntalString;
    }

    public static String getBreaString() {
        return BreaString;
    }

    public static void setBreaString(String breaString) {
        BreaString = breaString;
    }


    public static void setBfHeiPoun(String bfHeiPoun) {
        BfHeiPoun = bfHeiPoun;
    }

    public static Uri getData2() {
        return data2;
    }

    public static void setData2(Uri data2) {
        MainActivity.data2 = data2;
    }
}