package com.calendartemplate;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.SoundEffectConstants;
import android.view.View;
import android.widget.Button;

import com.datamedic.boyfrienddiary.R;

public class Setting extends MainActivity {


    Button about, policy, importt, export,select;
    BackUpRestore backUpRestore;
    public static final int KITKAT_VALUE = 1;
    String path = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settinglayout);

        backUpRestore = new BackUpRestore();

        about = findViewById(R.id.about);
        policy = findViewById(R.id.policy);
        importt = findViewById(R.id.importt);
        export = findViewById(R.id.export);
        select = findViewById(R.id.select);


        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), About.class);
                startActivity(intent);
                finish();
            }
        });

        policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/document/d/e/2PACX-1vRDluXaxHCSxkn5aewEpkJH1kokUASK4CJC6G2Mel-q_hmeUYs1vTmnSxPz81RHIPV0T37wnbRdg9sr/pub")));

            }
        });


        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setType("*/*");
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(intent, KITKAT_VALUE);


            }
        });



        importt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backUpRestore.importDB2(getApplicationContext(), path);

            }
        });

        export.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backUpRestore.exportDB2(getApplicationContext());

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    if (data != null) {
                        final Uri uri = data.getData();
                        try {
                            path = ResizeAndRotate.getPath(this, uri);

                        } catch (Exception e) {

                        }
                    }

                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}
