package com.calendartemplate;

public class CheckBoxClass {
    private String Name;
    private String Lname;
    private String Ck1;
    private String Ck2;
    private String Ck3;
    private String Ck4;
    private String Ck5;
    private String Ck6;
    private String Ck7;
    private String Ck8;
    private String Ck9;
    private String id;


    private String Ck1_b;
    private String Ck2_b;
    private String Ck3_b;
    private String Ck4_b;
    private String Ck5_b;
    private String Ck6_b;
    private String Ck7_b;

    public CheckBoxClass(String name, String lname, String ck1, String ck2, String ck3, String ck4, String ck5, String ck6, String ck7, String ck8, String ck9, String id,
                         String ck1_b,String ck2_b,String ck3_b,
                         String ck4_b,String ck5_b,String ck6_b,String ck7_b) {
        Name = name;
        Lname = lname;
        Ck1 = ck1;
        Ck2 = ck2;
        Ck3 = ck3;
        Ck4 = ck4;
        Ck5 = ck5;
        Ck6 = ck6;
        Ck7 = ck7;
        Ck8 = ck8;
        Ck9 = ck9;
        this.id = id;
        Ck1_b = ck1_b;
        Ck2_b = ck2_b;
        Ck3_b = ck3_b;
        Ck4_b = ck4_b;
        Ck5_b = ck5_b;
        Ck6_b = ck6_b;
        Ck7_b = ck7_b;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLname() {
        return Lname;
    }

    public void setLname(String lname) {
        Lname = lname;
    }

    public String getCk1() {
        return Ck1;
    }

    public void setCk1(String ck1) {
        Ck1 = ck1;
    }

    public String getCk2() {
        return Ck2;
    }

    public void setCk2(String ck2) {
        Ck2 = ck2;
    }

    public String getCk3() {
        return Ck3;
    }

    public void setCk3(String ck3) {
        Ck3 = ck3;
    }

    public String getCk4() {
        return Ck4;
    }

    public void setCk4(String ck4) {
        Ck4 = ck4;
    }

    public String getCk5() {
        return Ck5;
    }

    public void setCk5(String ck5) {
        Ck5 = ck5;
    }

    public String getCk6() {
        return Ck6;
    }

    public void setCk6(String ck6) {
        Ck6 = ck6;
    }

    public String getCk7() {
        return Ck7;
    }

    public void setCk7(String ck7) {
        Ck7 = ck7;
    }

    public String getCk8() {
        return Ck8;
    }

    public void setCk8(String ck8) {
        Ck8 = ck8;
    }
    public String getCk9() {
        return Ck9;
    }

    public void setCk9(String ck9) {
        Ck9 = ck9;
    }
    public String getid() {
        return id;
    }

    public void setBf_id(String bf_id) {
        this.id = bf_id;
    }
    public String getCk1_b() {
        return Ck1_b;
    }

    public void setCk1_b(String ck1_b) {
        Ck1_b = ck1_b;
    }

    public String getCk2_b() {
        return Ck2_b;
    }

    public void setCk2_b(String ck2_b) {
        Ck2_b = ck2_b;
    }

    public String getCk3_b() {
        return Ck3_b;
    }

    public void setCk3_b(String ck3_b) {
        Ck3_b = ck3_b;
    }

    public String getCk4_b() {
        return Ck4_b;
    }

    public void setCk4_b(String ck4_b) {
        Ck4_b = ck4_b;
    }

    public String getCk5_b() {
        return Ck5_b;
    }

    public void setCk5_b(String ck5_b) {
        Ck5_b = ck5_b;
    }

    public String getCk6_b() {
        return Ck6_b;
    }

    public void setCk6_b(String ck6_b) {
        Ck6_b = ck6_b;
    }

    public String getCk7_b() {
        return Ck7_b;
    }

    public void setCk7_b(String ck7_b) {
        Ck7_b = ck7_b;
    }







}
