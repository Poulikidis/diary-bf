package com.calendartemplate;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;

public class SaveId extends AppCompatActivity {
    public SharedPreferences sharedPreferences ;

    public SaveId(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }


    public void Save(String Id) {


        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("ID", Id);

    }

    public void TakeSaveId(String Id) {

        Id = sharedPreferences.getString("ID", "");
    }

}
