package com.calendartemplate;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.datamedic.boyfrienddiary.R;
import com.facebook.share.widget.ShareButton;

import java.util.ArrayList;

import static com.calendartemplate.MainActivity.getBfId;

public class CheckBoxList extends ArrayAdapter<CheckBoxClass> {

    public boolean Flag, Flag2;
    private Context mcontext;
    private int mResource;
    ShareButton fbShare;
    Button Delete, Update, SaveCb, Visibility, Visibility2;
    DataBase dataBase;
    CheckBox Fb1, Ph1, Inst1, Dt1, Ks1, Mo11, Sx1,Je,Can,Nag,Ind,Chea,Ntal,Break;
    EditText Com1;


    public CheckBoxList(@NonNull Context context, int resource, ArrayList objects) {
        super(context, resource, objects);

        mcontext = context;
        mResource = resource;
        dataBase = new DataBase(context);



    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        final String Name = getItem(position).getName();
        final String LName = getItem(position).getLname();
        final String Calendar = getItem(position).getCk1();
        final String Fb = getItem(position).getCk2();
        final String Phone = getItem(position).getCk3();
        final String Inst = getItem(position).getCk4();
        final String Date = getItem(position).getCk5();
        final String Kiss = getItem(position).getCk6();
        final String Mo = getItem(position).getCk7();
        final String Sex = getItem(position).getCk8();
        final String Commend = getItem(position).getCk9();
        final String ID = getItem(position).getid();
        final String Fight = getItem(position).getCk1_b();
        final String Cancel = getItem(position).getCk2_b();
        final String Insult = getItem(position).getCk3_b();
        final String DisablePhone = getItem(position).getCk4_b();
        final String Cheating = getItem(position).getCk5_b();
        final String Reject = getItem(position).getCk6_b();
        final String Breaking = getItem(position).getCk7_b();


        final LayoutInflater inflater = LayoutInflater.from(mcontext);
        convertView = inflater.inflate(mResource, parent, false);

        TextView NameText = convertView.findViewById(R.id.ckLname);
        TextView CaleText = convertView.findViewById(R.id.ck1);
        TextView CommendText = convertView.findViewById(R.id.ck9);
        final ImageView fbImg = convertView.findViewById(R.id.FacebookIMG);
        final ImageView phoneImg = convertView.findViewById(R.id.PhoneIMG);
        final ImageView instaImg = convertView.findViewById(R.id.InstagramIMG);
        ImageView dateImg = convertView.findViewById(R.id.DateIMG);
        ImageView kissImg = convertView.findViewById(R.id.KissIMG);
        ImageView moImg = convertView.findViewById(R.id.MakeoutIMG);
        ImageView sexImg = convertView.findViewById(R.id.SexIMG);
        ImageView fightImg = convertView.findViewById(R.id.FightIMG);
        ImageView cancelImg = convertView.findViewById(R.id.DateCanceledIMG);
        ImageView insultImg = convertView.findViewById(R.id.InsultIMG);
        ImageView disableImg = convertView.findViewById(R.id.PowerOffIMG);
        ImageView cheatImg = convertView.findViewById(R.id.CheatingIMG);
        ImageView rejectImg = convertView.findViewById(R.id.RejectCallIMG);
        ImageView breakImg = convertView.findViewById(R.id.BrokeUpIMG);
        Delete = convertView.findViewById(R.id.Delete);
        Update = convertView.findViewById(R.id.Update);
        fbShare = convertView.findViewById(R.id.fbShare);
        fbShare.setEnabled(true);

        fbShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent
                        .putExtra(Intent.EXTRA_TEXT,
                                "Diary Log Date : "  +  Calendar + "\n\nMy notes : \n"+ Commend +   " \n\n\nBoyfriend Diary App --> Get it on Google Play");
                sendIntent.setType("text/plain");
                sendIntent.setPackage("com.facebook.orca");
                try {
                    mcontext.startActivity(sendIntent);
                }
                catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(mcontext,"Please Install Facebook Messenger", Toast.LENGTH_LONG).show();
                }
            }
        });

        Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = ((LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE));
                View customView1 = inflater.inflate(R.layout.delete, null, false);
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(mcontext);


                Button Yes = customView1.findViewById(R.id.YesDelete);
                Button No = customView1.findViewById(R.id.NoDelete);
                mBuilder.setView(customView1);
                final AlertDialog dialog = mBuilder.create();

                Yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dataBase.deleteCkFron(ID);
                        Intent i = new Intent(mcontext, FrontList.class);
                        mcontext.startActivity(i);
                        dialog.cancel();

                    }
                });
                No.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();

                    }
                });


                dialog.show();


            }
        });


        Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = ((LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE));
                final View customView1 = inflater.inflate(R.layout.checkbox, null, false);
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(mcontext);


                final ConstraintLayout constraintLayout = customView1.findViewById(R.id.Expandable);
                constraintLayout.setVisibility(customView1.GONE);

                final ConstraintLayout constraintLayout2 = customView1.findViewById(R.id.Expandable2);
                constraintLayout2.setVisibility(customView1.GONE);


                Visibility = customView1.findViewById(R.id.Viss);
                Visibility2 = customView1.findViewById(R.id.Viss2);

                Visibility.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (!Flag) {
                            constraintLayout2.setVisibility(customView1.GONE);
                            constraintLayout.setVisibility(customView1.VISIBLE);

                            Flag = true;
                            Flag2 = false;
                        } else {
                            constraintLayout.setVisibility(customView1.GONE);
                            Flag = false;
                        }


                    }

                });





                Visibility2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (!Flag2) {
                            constraintLayout2.setVisibility(customView1.VISIBLE);
                            constraintLayout.setVisibility(customView1.GONE);
                            Flag2 = true;
                            Flag = false;
                        } else {
                            constraintLayout2.setVisibility(customView1.GONE);
                            Flag2 = false;
                        }

                    }

                });


                Fb1 = customView1.findViewById(R.id.Fb);
                Ph1 = customView1.findViewById(R.id.Ph);
                Inst1 = customView1.findViewById(R.id.Inst);
                Dt1 = customView1.findViewById(R.id.Dt);
                Ks1 = customView1.findViewById(R.id.Ks);
                Mo11 = customView1.findViewById(R.id.Mo);
                Sx1 = customView1.findViewById(R.id.Sx);
                Com1 = customView1.findViewById(R.id.Comment);
                SaveCb = customView1.findViewById(R.id.BtSaveCb);
                Je = customView1.findViewById(R.id.Fight);
                Can = customView1.findViewById(R.id.Cancel);
                Nag = customView1.findViewById(R.id.insult);
                Ind = customView1.findViewById(R.id.disablephone);
                Chea = customView1.findViewById(R.id.Cheat);
                Ntal = customView1.findViewById(R.id.Reject);
                Break = customView1.findViewById(R.id.Break);

                if (Fb.equals(mcontext.getString(R.string.Emt)))
                    Fb1.setChecked(false);
                else Fb1.setChecked(true);

                if (Phone.equals(mcontext.getString(R.string.Emt)))
                    Ph1.setChecked(false);
                else Ph1.setChecked(true);

                if (Inst.equals(mcontext.getString(R.string.Emt)))
                    Inst1.setChecked(false);
                else Inst1.setChecked(true);

                if (Date.equals(mcontext.getString(R.string.Emt)))
                    Dt1.setChecked(false);
                else Dt1.setChecked(true);

                if (Kiss.equals(mcontext.getString(R.string.Emt)))
                    Ks1.setChecked(false);
                else Ks1.setChecked(true);

                if (Mo.equals(mcontext.getString(R.string.Emt)))
                    Mo11.setChecked(false);
                else Mo11.setChecked(true);

                if (Sex.equals(mcontext.getString(R.string.Emt)))
                    Sx1.setChecked(false);
                else Sx1.setChecked(true);

                if (Fight.equals(mcontext.getString(R.string.Emt)))
                    Je.setChecked(false);
                else Je.setChecked(true);

                if (Cancel.equals(mcontext.getString(R.string.Emt)))
                    Chea.setChecked(false);
                else Chea.setChecked(true);

                if (Insult.equals(mcontext.getString(R.string.Emt)))
                    Nag.setChecked(false);
                else Nag.setChecked(true);

                if (DisablePhone.equals(mcontext.getString(R.string.Emt)))
                    Ind.setChecked(false);
                else Ind.setChecked(true);

                if (Cheating.equals(mcontext.getString(R.string.Emt)))
                    Chea.setChecked(false);
                else Chea.setChecked(true);

                if (Reject.equals(mcontext.getString(R.string.Emt)))
                    Ntal.setChecked(false);
                else Ntal.setChecked(true);

                if (Breaking.equals(mcontext.getString(R.string.Emt)))
                    Break.setChecked(false);
                else Break.setChecked(true);


                Com1.setText(Commend);


                if (Fb1.isChecked())
                    MainActivity.setFbString(mcontext.getString(R.string.FB));
                if (Ph1.isChecked())
                    MainActivity.setPhString(mcontext.getString(R.string.Phone));
                if (Inst1.isChecked())
                    MainActivity.setInstString(mcontext.getString(R.string.Ins));
                if (Dt1.isChecked())
                    MainActivity.setDtString(mcontext.getString(R.string.Date));
                if (Ks1.isChecked())
                    MainActivity.setKsString(mcontext.getString(R.string.Kiss));
                if (Mo11.isChecked())
                    MainActivity.setMoString(mcontext.getString(R.string.Makeout));
                if (Sx1.isChecked())
                    MainActivity.setSxString(mcontext.getString(R.string.Sex));
                if (Je.isChecked())
                    MainActivity.setJeString(mcontext.getString(R.string.Fight));
                if (Can.isChecked())
                    MainActivity.setCanString(mcontext.getString(R.string.Cancel));
                if (Nag.isChecked())
                    MainActivity.setNagString(mcontext.getString(R.string.insult));
                if (Ind.isChecked())
                    MainActivity.setIndString(mcontext.getString(R.string.disablephone));
                if (Chea.isChecked())
                    MainActivity.setCheaString(mcontext.getString(R.string.Cheating));
                if (Ntal.isChecked())
                    MainActivity.setNtalString(mcontext.getString(R.string.Reject));
                if (Break.isChecked())
                    MainActivity.setBreaString(mcontext.getString(R.string.Breaking));

                if (dataBase.chFace(getBfId())) {
                    Fb1.setEnabled(true);
                } else {
                    Fb1.setEnabled(false);
                    fbImg.setImageURI(Uri.parse("android.resource://com.calendartemplate/drawable/facebookgrey"));
                }

                Fb1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                        if (isChecked) {
                            MainActivity.setFbString(mcontext.getString(R.string.FB));


                        } else MainActivity.setFbString(mcontext.getString(R.string.Emt));

                    }
                });

                if (dataBase.chPhone(getBfId())) {
                    Ph1.setEnabled(true);
                } else {
                    Ph1.setEnabled(false);
                    phoneImg.setImageURI(Uri.parse("android.resource://com.calendartemplate/drawable/phonegrey"));
                }
                Ph1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                        if (isChecked) {
                            MainActivity.setPhString(mcontext.getString(R.string.Phone));


                        } else MainActivity.setPhString(mcontext.getString(R.string.Emt));
                    }
                });

                if (dataBase.chInsta(getBfId())) {
                    Inst1.setEnabled(true);
                } else {
                    Inst1.setEnabled(false);
                    instaImg.setImageURI(Uri.parse("android.resource://com.calendartemplate/drawable/instagramgrey"));
                }
                Inst1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                        if (isChecked) {
                            MainActivity.setInstString(mcontext.getString(R.string.Ins));


                        } else MainActivity.setInstString(mcontext.getString(R.string.Emt));
                    }
                });

                Dt1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                        if (isChecked) {
                            MainActivity.setDtString(mcontext.getString(R.string.Date));


                        } else MainActivity.setDtString(mcontext.getString(R.string.Emt));
                    }
                });

                Ks1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                        if (isChecked) {
                            MainActivity.setKsString(mcontext.getString(R.string.Kiss));


                        } else MainActivity.setKsString(mcontext.getString(R.string.Emt));
                    }
                });

                Mo11.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            MainActivity.setMoString(mcontext.getString(R.string.Makeout));


                        } else MainActivity.setMoString(mcontext.getString(R.string.Emt));
                    }
                });

                Sx1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                        if (isChecked) {
                            MainActivity.setSxString(mcontext.getString(R.string.Sex));


                        } else MainActivity.setSxString(mcontext.getString(R.string.Emt));
                    }
                });
                Je.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            MainActivity.setJeString(mcontext.getString(R.string.Fight));


                        } else MainActivity.setJeString(mcontext.getString(R.string.Emt));
                    }
                });

                Can.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            MainActivity.setCanString(mcontext.getString(R.string.Cancel));


                        } else MainActivity.setCanString(mcontext.getString(R.string.Emt));
                    }
                });

                Nag.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            MainActivity.setNagString(mcontext.getString(R.string.insult));


                        } else MainActivity.setNagString(mcontext.getString(R.string.Emt));
                    }
                });


                Ind.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            MainActivity.setIndString(mcontext.getString(R.string.disablephone));


                        } else MainActivity.setIndString(mcontext.getString(R.string.Emt));
                    }
                });

                Chea.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            MainActivity.setCheaString(mcontext.getString(R.string.Cheating));


                        } else MainActivity.setCheaString(mcontext.getString(R.string.Emt));
                    }
                });

                Ntal.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            MainActivity.setNtalString(mcontext.getString(R.string.Reject));


                        } else MainActivity.setNtalString(mcontext.getString(R.string.Emt));
                    }
                });
                Break.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                        if (isChecked) {
                            MainActivity.setBreaString(mcontext.getString(R.string.Breaking));


                        } else MainActivity.setBreaString(mcontext.getString(R.string.Emt));

                    }
                });

                mBuilder.setView(customView1);
                final AlertDialog dialog = mBuilder.create();
                dialog.show();

                if (getBfId() == null) {
                    Toast.makeText(mcontext, "Please select boyfriend", Toast.LENGTH_SHORT).show();
                } else {
                    dialog.show();
                }

                SaveCb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        boolean inserted = dataBase.updateCk(ID, Calendar, MainActivity.getFbString(), MainActivity.getPhString(), MainActivity.getInstString(),
                                MainActivity.getDtString(), MainActivity.getKsString(),
                                MainActivity.getMoString(), MainActivity.getSxString(), Com1.getText().toString(), getBfId(), MainActivity.getJeString(),
                                MainActivity.getCanString(), MainActivity.getNagString(), MainActivity.getIndString(), MainActivity.getCheaString(), MainActivity.getNtalString()
                                , MainActivity.getBreaString());

                        if (inserted) {
                            Toast.makeText(mcontext, "Inserted", Toast.LENGTH_SHORT).show();

                            dialog.cancel();
                            Intent i = new Intent(mcontext, FrontList.class);

                            mcontext.startActivity(i);


                            MainActivity.setFbString(mcontext.getString(R.string.Emt));
                            MainActivity.setPhString(mcontext.getString(R.string.Emt));
                            MainActivity.setInstString(mcontext.getString(R.string.Emt));
                            MainActivity.setDtString(mcontext.getString(R.string.Emt));
                            MainActivity.setKsString(mcontext.getString(R.string.Emt));
                            MainActivity.setMoString(mcontext.getString(R.string.Emt));
                            MainActivity.setSxString(mcontext.getString(R.string.Emt));
                            MainActivity.setJeString(mcontext.getString(R.string.Emt));
                            MainActivity.setCanString(mcontext.getString(R.string.Emt));
                            MainActivity.setNagString(mcontext.getString(R.string.Emt));
                            MainActivity.setIndString(mcontext.getString(R.string.Emt));
                            MainActivity.setCheaString(mcontext.getString(R.string.Emt));
                            MainActivity.setNtalString(mcontext.getString(R.string.Emt));
                            MainActivity.setBreaString(mcontext.getString(R.string.Emt));


                        } else
                            Toast.makeText(mcontext, " Not Inserted", Toast.LENGTH_SHORT).show();


                    }
                });

            }

        });


        if (!dataBase.chFace(ID))
            fbImg.setImageURI(Uri.parse("android.resource://com.datamedic.boyfrienddiary/drawable/facebook"));
        if (!dataBase.chPhone(ID))
            phoneImg.setImageURI(Uri.parse("android.resource://com.datamedic.boyfrienddiary/drawable/phone"));
        if (!dataBase.chInsta(ID))
            instaImg.setImageURI(Uri.parse("android.resource://com.datamedic.boyfrienddiary/drawable/instagram"));
        if (Date.equals("Date"))
            dateImg.setImageURI(Uri.parse("android.resource://com.datamedic.boyfrienddiary/drawable/date"));
        if (Kiss.equals("Kiss"))
            kissImg.setImageURI(Uri.parse("android.resource://com.datamedic.boyfrienddiary/drawable/kiss"));
        if (Mo.equals("Make Out"))
            moImg.setImageURI(Uri.parse("android.resource://com.datamedic.boyfrienddiary/drawable/makeout"));
        if (Sex.equals("Sex"))
            sexImg.setImageURI(Uri.parse("android.resource://com.datamedic.boyfrienddiary/drawable/sex"));
        if (Fight.equals("Fight"))
            fightImg.setImageURI(Uri.parse("android.resource://com.datamedic.boyfrienddiary/drawable/fight"));
        if (Cancel.equals("Cancel date"))
            cancelImg.setImageURI(Uri.parse("android.resource://com.datamedic.boyfrienddiary/drawable/datecanceled"));
        if (Insult.equals("Insulted"))
            insultImg.setImageURI(Uri.parse("android.resource://com.datamedic.boyfrienddiary/drawable/insult"));
        if (DisablePhone.equals("disablephone"))
            disableImg.setImageURI(Uri.parse("android.resource://com.datamedic.boyfrienddiary/drawable/poweroff"));
        if (Cheating.equals("Cheating"))
            cheatImg.setImageURI(Uri.parse("android.resource://com.datamedic.boyfrienddiary/drawable/cheating"));
        if (Reject.equals("Reject"))
            rejectImg.setImageURI(Uri.parse("android.resource://com.datamedic.boyfrienddiary/drawable/rejectcall"));
        if (Breaking.equals("Breaking up"))
            breakImg.setImageURI(Uri.parse("android.resource://com.datamedic.boyfrienddiary/drawable/brakeup"));


        NameText.setText(Name + "  " + LName);
        CaleText.setText(Calendar);
        CommendText.setText(Commend);


        return convertView;
    }
}
