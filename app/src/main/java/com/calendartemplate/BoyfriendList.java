package com.calendartemplate;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.datamedic.boyfrienddiary.R;

import java.util.ArrayList;

public class BoyfriendList extends MainActivity {
    Button AddBfDialog;
    Button BackList;
    Button AddBoyfriend;
    Button Update;
    static Button Delete;
    DataBase dataBase;
    EditText Name, LName, Height, Pounds,Age;
    Button AdImage;

    PersonList personList;
    public static final int KITKAT_VALUE = 1002;
    private AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.5F);

    public static Uri data1;







    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.list_layout);

        dataBase = new DataBase(this);
        AddBfDialog = findViewById(R.id.AddBf);
        BackList = findViewById(R.id.BackList);
        Update = findViewById(R.id.Update);
        final ListView list = findViewById(R.id.List);
        final Cursor res = dataBase.getAll();


        final ArrayList<BoyfriendClass> theList = new ArrayList<>();


        if (res.getCount() == 0) {
            Toast.makeText(BoyfriendList.this, " Nothing in Database", Toast.LENGTH_SHORT).show();

        } else
            while (res.moveToNext()) {
            BoyfriendClass boyfriend = new BoyfriendClass(res.getString(1), res.getString(2), res.getString(3), res.getString(4)
                        , res.getString(5),res.getString(6), res.getString(0));



                theList.add(boyfriend);
                personList = new PersonList(this, R.layout.listlayout, theList);
                list.setAdapter(personList);


            }


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                MainActivity.BfFocus = theList.get(position).getName() + "  " + theList.get(position).getLName();
                Toast.makeText(getApplicationContext(), BfFocus, Toast.LENGTH_SHORT).show();
                MainActivity.setBfId(theList.get(position).getId());
                MainActivity.BfHeiPoun = theList.get(position).getHeight() + "  " + theList.get(position).getPounds();
                MainActivity.data2 = Uri.parse(theList.get(position).getImage());
                SharedPreferences.Editor editor = getSharedPreferences("MyPrefs",MODE_PRIVATE).edit();
                editor.putString("BfId", MainActivity.getBfId());
                editor.putString("BfNL", MainActivity.BfFocus);
                editor.putString("BfHP", MainActivity.BfHeiPoun);
                editor.putString("BfIm", MainActivity.data2.toString());
                editor.apply();
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(),MainActivity.class);
                startActivity(intent);
                finish();



            }
        });


        AddBfDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(buttonClick);

                AlertDialog.Builder mBuilder = new AlertDialog.Builder(BoyfriendList.this);
                View mView = getLayoutInflater().inflate(R.layout.addboyfriend, null);
                AddBoyfriend = mView.findViewById(R.id.AddButton);
                Name = mView.findViewById(R.id.Name);
                LName = mView.findViewById(R.id.LastName);
                Height = mView.findViewById(R.id.Height);
                Pounds = mView.findViewById(R.id.Pounds);
                Age = mView.findViewById(R.id.age);
                AdImage = mView.findViewById(R.id.button);

                Name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            Name.setHint("Name");
                        } else {
                            Name.setHint("");
                        }
                    }
                });
                LName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            LName.setHint("Last Name");
                        } else {
                            LName.setHint("");
                        }
                    }
                });
                Height.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            Height.setHint("Height");
                        } else {
                            Height.setHint("");
                        }
                    }
                });
                Pounds.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            Pounds.setHint("Pounds");
                        } else {
                            Pounds.setHint("");
                        }
                    }
                });
                Age.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            Age.setHint("Age");
                        } else {
                            Age.setHint("");
                        }
                    }
                });
                AdImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (Build.VERSION.SDK_INT < 19) {
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_PICK);
                            intent.setType("image/*");
                            startActivityForResult(intent, KITKAT_VALUE);
                        } else {
                            Intent
                                    intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                            intent.addCategory(Intent.CATEGORY_OPENABLE);
                            intent.setType("image/*");
                            startActivityForResult(intent, KITKAT_VALUE);
                        }

                    }
                });


                mBuilder.setView(mView);
                final AlertDialog dialog = mBuilder.create();
                dialog.show();

                AddBoyfriend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.startAnimation(buttonClick);
                        if (data1 != null) {
                            boolean insertet = dataBase.data(Name.getText().toString(), LName.getText().toString(), Height.getText().toString(), Pounds.getText().toString(),Age.getText().toString()
                                    , data1.toString());
                            if (insertet) {
                                Toast.makeText(BoyfriendList.this, "Inserted", Toast.LENGTH_SHORT).show();
                                SharedPreferences.Editor editor = getSharedPreferences("MyPrefs",MODE_PRIVATE).edit();
                                editor.putString("BfId", String.valueOf(dataBase.getLastId()));
                                editor.putString("BfNL",Name.getText() + "  " + LName.getText());
                                editor.putString("BfHP", Height.getText() + "  " + Pounds.getText());
                                editor.putString("BfIm", data1.toString());
                                editor.apply();

                                dialog.cancel();
                                list.invalidateViews();
                                Intent i = new Intent(BoyfriendList.this, MainActivity.class);



                                startActivity(i);
                                finish();
                            }else
                                Toast.makeText(BoyfriendList.this, " Opps Nothing Saved", Toast.LENGTH_SHORT).show();

                        }


                        if (data1 == null) {

                            data1 = Uri.parse("android.resource://com.datamedic.boyfrienddiary/drawable/emptyface");
                            boolean insertet = dataBase.data(Name.getText().toString(), LName.getText().toString(), Height.getText().toString(), Pounds.getText().toString(),Age.getText().toString()
                                    , data1.toString());
                            if (insertet) {
                                Toast.makeText(BoyfriendList.this, "Inserted", Toast.LENGTH_SHORT).show();
                                SharedPreferences.Editor editor = getSharedPreferences("MyPrefs",MODE_PRIVATE).edit();
                                editor.putString("BfId", String.valueOf(dataBase.getLastId()));
                                editor.putString("BfNL",Name.getText() + "  " + LName.getText());
                                editor.putString("BfHP", Height.getText() + "  " + Pounds.getText());
                                editor.putString("BfIm", data1.toString());
                                editor.apply();
                                dialog.cancel();
                                list.invalidateViews();
                                Intent i = new Intent(BoyfriendList.this, MainActivity.class);

                                startActivity(i);
                                finish();
                            }else
                                Toast.makeText(BoyfriendList.this, " Opps Nothing Saved", Toast.LENGTH_SHORT).show();

                        }


                        data1 = null;
                    }
                });


            }
        });


        BackList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(buttonClick);
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
            }
        });



    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(),MainActivity.class);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == KITKAT_VALUE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                data1 = data.getData();
            }
        }

    }



}
