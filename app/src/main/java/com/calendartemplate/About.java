package com.calendartemplate;

import android.content.Intent;
import android.os.Bundle;

import com.datamedic.boyfrienddiary.R;

public class About extends MainActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);


    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(),Setting.class);
        finish();
        super.onBackPressed();
    }


}
