package com.calendartemplate;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.datamedic.boyfrienddiary.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

public class FrontList extends MainActivity {


    DataBase dataBase;
    ListView listView;
    CheckBoxList checkBoxList;

    TextView displayFrom, displayTo,textfrom,textto;
    DatePickerDialog.OnDateSetListener mDateListener, mDateListener2;

    Calendar cal;
    Calendar cal2;
    Button select;
    private AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.5F);


    public static long calendarIntFrom;
    public static long calendarIntTo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frontlist);
        dataBase = new DataBase(this);
        Cursor res = dataBase.innerJoin(getBfId());


        listView = findViewById(R.id.ListFront);
        textfrom = findViewById(R.id.textView10);
        textto = findViewById(R.id.textView11);


        displayFrom = findViewById(R.id.DisplayFrom);

        displayFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(buttonClick);
                cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog dialog = new DatePickerDialog(FrontList.this, android.R.style.Theme_Holo_Dialog, mDateListener, year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();


            }
        });
        mDateListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                cal.set(year,month,dayOfMonth,00,00);
                calendarIntFrom = cal.getTimeInMillis();
                Log.d("gianadoum",calendarIntFrom + "");
                month += 1;
                String date = dayOfMonth + "/" + month + "/" + year;
                textfrom.setText(date);


            }
        };




        displayTo = findViewById(R.id.DisplayTo);

        displayTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(buttonClick);
                cal2 = Calendar.getInstance();
                int year = cal2.get(Calendar.YEAR);
                int month = cal2.get(Calendar.MONTH);
                int day = cal2.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(FrontList.this, android.R.style.Theme_Holo_Dialog, mDateListener2, year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

            }
        });
        mDateListener2 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                cal2.set(year,month,dayOfMonth,23,59);
                calendarIntTo = cal2.getTimeInMillis();
                Log.d("gianadoum",calendarIntTo + "");
                month += 1;
                String Date = dayOfMonth + "/" + month + "/" + year;
                textto.setText(Date);


            }
        };


        final ArrayList<CheckBoxClass> theList = new ArrayList<>();
        if (res.getCount() == 0)
            Toast.makeText(FrontList.this, " Nothing Stored in Database", Toast.LENGTH_SHORT);
        else

            res.moveToLast();
        res.moveToNext();
            while (res.moveToPrevious()) {
                CheckBoxClass checkBoxClass = new CheckBoxClass(res.getString(1), res.getString(2)
                        , res.getString(8), res.getString(9)
                        , res.getString(10), res.getString(11), res.getString(12)
                        , res.getString(13), res.getString(14), res.getString(15), res.getString(16), res.getString(7), res.getString(18),
                        res.getString(19), res.getString(20), res.getString(21), res.getString(22), res.getString(23), res.getString(24));
                theList.add(checkBoxClass);

                checkBoxList = new CheckBoxList(this, R.layout.cklist, theList);
                listView.setAdapter(checkBoxList);
//                Collections.reverse(theList);
            }





        select = findViewById(R.id.select);

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(buttonClick);
                Intent intent = new Intent();
                intent.setClass(FrontList.this,SelectList.class);
                startActivity(intent);
                finish();




            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }

    public static long getCalendarIntFrom() {
        return calendarIntFrom;
    }

    public void setCalendarIntFrom(long calendarIntFrom) {
        this.calendarIntFrom = calendarIntFrom;
    }

    public static long getCalendarIntTo() {
        return calendarIntTo;
    }

    public void setCalendarIntTo(long calendarIntTo) {
        this.calendarIntTo = calendarIntTo;
    }
}
