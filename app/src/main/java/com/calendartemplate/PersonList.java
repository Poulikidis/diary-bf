package com.calendartemplate;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.datamedic.boyfrienddiary.R;
import java.io.IOException;
import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;





public class PersonList extends ArrayAdapter<BoyfriendClass> {
    private Context mcontext;
    public  int KITKAT_VALUE = 1002;




    Button Delete, Update;
    private int mresource;
    DataBase dataBase;
    Bitmap bitmap;

    BoyfriendList boyfriendList = new BoyfriendList();
    ResizeAndRotate resizeAndRotate;



    public PersonList(@NonNull Context context, int resource, ArrayList object) {
        super(context, resource, object);
        mcontext = context;
        mresource = resource;
        dataBase = new DataBase(context);
        resizeAndRotate = new ResizeAndRotate();







    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final String Name = getItem(position).getName();
        final String Lname = getItem(position).getLName();
        final String Height = getItem(position).getHeight();
        final String Pounds = getItem(position).getPounds();
        final String Age = getItem(position).getAge();
        final String Image = getItem(position).getImage();
        final String ID = getItem(position).getId();




        final LayoutInflater inflater = LayoutInflater.from(mcontext);
        convertView = inflater.inflate(mresource, parent, false);

        TextView NameText = convertView.findViewById(R.id.textView5);
        TextView LNameText = convertView.findViewById(R.id.textView6);
        TextView HeightText = convertView.findViewById(R.id.textView7);
        TextView PoundsText = convertView.findViewById(R.id.textView8);
        TextView AgeText = convertView.findViewById(R.id.ageText);
        ImageView ImagePic = convertView.findViewById(R.id.imageView);
        ImageView FbPic = convertView.findViewById(R.id.FacebookIMG);
        ImageView PhPic = convertView.findViewById(R.id.PhoneIMG);
        ImageView IntPic = convertView.findViewById(R.id.InstagramIMG);

        if (!dataBase.chFace(ID))
            FbPic.setImageURI(Uri.parse("android.resource://com.datamedic.boyfrienddiary/drawable/facebook"));
        if (!dataBase.chPhone(ID))
            PhPic.setImageURI(Uri.parse("android.resource://com.datamedic.boyfrienddiary/drawable/phone"));
        if (!dataBase.chInsta(ID))
            IntPic.setImageURI(Uri.parse("android.resource://com.datamedic.boyfrienddiary/drawable/instagram"));

        Delete = convertView.findViewById(R.id.Delete);
        Update = convertView.findViewById(R.id.Update);


        Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = ((LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE));
                View customView1 = inflater.inflate(R.layout.delete, null, false);
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(mcontext);


                Button Yes = customView1.findViewById(R.id.YesDelete);
                Button No = customView1.findViewById(R.id.NoDelete);
                mBuilder.setView(customView1);
                final AlertDialog dialog = mBuilder.create();

                Yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dataBase.delete(ID);
                        dataBase.deleteCk(ID);
                        SharedPreferences.Editor editor = mcontext.getSharedPreferences("MyPrefs",MODE_PRIVATE).edit();
                        editor.clear().commit();
                        Intent i = new Intent(mcontext, BoyfriendList.class);
                        mcontext.startActivity(i);
                        dialog.cancel();

                    }
                });
                No.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();

                    }
                });


                dialog.show();


            }
        });
        Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = ((LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE));
                View customView1 = inflater.inflate(R.layout.addboyfriend, null, false);
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(mcontext);


                if (MainActivity.getBfId() != null) {

                    final Button AddBoyfriend = customView1.findViewById(R.id.AddButton);
                    final EditText Name1 = customView1.findViewById(R.id.Name);
                    final EditText LName1 = customView1.findViewById(R.id.LastName);
                    final EditText Height1 = customView1.findViewById(R.id.Height);
                    final EditText Pounds1 = customView1.findViewById(R.id.Pounds);
                    final EditText Age1 = customView1.findViewById(R.id.age);
                    final Button AdImage1 = customView1.findViewById(R.id.button);

                    Name1.setText(Name);
                    LName1.setText(Lname);
                    Height1.setText(Height);
                    Pounds1.setText(Pounds);
                    Age1.setText(Age);
                    boyfriendList.data1 = Uri.parse(Image);


                    Name1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                Name1.setHint("Name");
                            } else {
                                Name1.setHint("");
                            }
                        }
                    });
                    LName1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                LName1.setHint("Last Name");
                            } else {
                                LName1.setHint("");
                            }
                        }
                    });
                    Height1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                Height1.setHint("Height");
                            } else {
                                Height1.setHint("");
                            }
                        }
                    });
                    Pounds1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                Pounds1.setHint("Weight");
                            } else {
                                Pounds1.setHint("");
                            }
                        }
                    });
                    Age1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View v, boolean hasFocus) {
                            if (hasFocus) {
                                Age1.setHint("Age");
                            } else {
                                Age1.setHint("");
                            }
                        }
                    });
                    AdImage1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {



                            if (Build.VERSION.SDK_INT < 19) {
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_PICK);
                                intent.setType("*/*");
                                if (mcontext instanceof Activity) {
                                    ((Activity) mcontext).startActivityForResult(intent,KITKAT_VALUE);
                                } else {
                                    Toast.makeText(mcontext, " la8os",Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                                intent.addCategory(Intent.CATEGORY_OPENABLE);
                                intent.setType("*/*");
                                if (mcontext instanceof Activity) {
                                    ((Activity) mcontext).startActivityForResult(intent,KITKAT_VALUE);
                                } else {
                                    Toast.makeText(mcontext, " la8os",Toast.LENGTH_SHORT).show();
                                }
                            }

                        }
                    });


                    mBuilder.setView(customView1);
                    final AlertDialog dialog = mBuilder.create();
                    dialog.show();

                    AddBoyfriend.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (boyfriendList.data1 != null) {
                                boolean insertet = dataBase.update(ID, Name1.getText().toString(), LName1.getText().toString(), Height1.getText().toString(), Pounds1.getText().toString(),Age1.getText().toString()
                                        , boyfriendList.data1.toString());
                                if (insertet) {
                                    Toast.makeText(mcontext, "Boyfriend Data Saved", Toast.LENGTH_SHORT).show();
                                    SharedPreferences.Editor editor = mcontext.getSharedPreferences("MyPrefs",MODE_PRIVATE).edit();
                                    editor.putString("BfId", ID);
                                    editor.putString("BfNL", Name1.getText() + "  " + LName1.getText());
                                    editor.putString("BfHP",Height1.getText() + "  " + Pounds1.getText());
                                    editor.putString("BfIm", BoyfriendList.data1.toString());
                                    editor.apply();
                                    dialog.cancel();
                                    Intent i = new Intent(mcontext, BoyfriendList.class);
                                    mcontext.startActivity(i);

                                } else
                                    Toast.makeText(mcontext, "Boyfriend Data Not Saved", Toast.LENGTH_SHORT).show();

                            }


                            if (boyfriendList.data1 == null) {
                                boyfriendList.data1 = Uri.parse("android.resource://com.datamedic.boyfrienddiary/drawable/faceEmpty");
                                boolean insertet = dataBase.update(ID, Name1.getText().toString(), LName1.getText().toString(), Height1.getText().toString(), Pounds1.getText().toString(),Age1.getText().toString()
                                        , boyfriendList.data1.toString());
                                if (insertet) {
                                    Toast.makeText(mcontext, "Boyfriend Data Saved", Toast.LENGTH_SHORT).show();
                                    SharedPreferences.Editor editor = mcontext.getSharedPreferences("MyPrefs",MODE_PRIVATE).edit();
                                    editor.putString("BfId", ID);
                                    editor.putString("BfNL", Name1.getText() + "  " + LName1.getText());
                                    editor.putString("BfHP",Height1.getText() + "  " + Pounds1.getText());
                                    editor.putString("BfIm", BoyfriendList.data1.toString());
                                    editor.apply();
                                    dialog.cancel();

                                    Intent i = new Intent(mcontext, BoyfriendList.class);

                                    mcontext.startActivity(i);


                                } else
                                    Toast.makeText(mcontext, "Boyfriend Data Not Saved", Toast.LENGTH_SHORT).show();

                            }


                            boyfriendList.data1 = null;
                        }
                    });


                } else
                    Toast.makeText(mcontext, " No Boyfriend Checked", Toast.LENGTH_SHORT).show();


            }
        });


        NameText.setText(Name);
        LNameText.setText(Lname);
        HeightText.setText( Height);
        PoundsText.setText(Pounds);
        AgeText.setText(Age);


        Uri uri = Uri.parse(Image);
        bitmap = BitmapFactory.decodeFile(resizeAndRotate.getPath(mcontext,uri));

        String path = "";
        path = resizeAndRotate.getPath(mcontext,uri);
        if (path != null) {
            try {
               resizeAndRotate.getResizedBitmap(bitmap,20,20);
                ImagePic.setImageBitmap(resizeAndRotate.modifyOrientation(bitmap, resizeAndRotate.getPath(mcontext,uri)));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else ImagePic.setImageURI(uri);




        return convertView;




    }








}
