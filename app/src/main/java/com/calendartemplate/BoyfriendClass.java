package com.calendartemplate;

public class BoyfriendClass{
    private String Name;
    private String LName;
    private String Height;
    private String Pounds;
    private String Age;
    private String Image;
    private String Id;

    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        Age = age;
    }


    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLName() {
        return LName;
    }

    public void setLName(String LName) {
        this.LName = LName;
    }

    public String getHeight() {
        return Height;
    }

    public void setHeight(String height) {
        Height = height;
    }

    public String getPounds() {
        return Pounds;
    }

    public void setPounds(String pounds) {
        Pounds = pounds;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }
    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public BoyfriendClass(String name, String LName, String height, String pounds,String age,String Image,String id) {
        Name = name;
        this.LName = LName;
        Height = height;
        Pounds = pounds;
        Age = age;
        this.Image = Image;
        Id = id;
    }
}
