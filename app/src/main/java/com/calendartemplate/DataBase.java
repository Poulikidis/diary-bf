package com.calendartemplate;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataBase extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Boyfriend.db";
    public static final String BF_TABLE = "Boyfriend_table";
    public static final String ID = "ID";
    public static final String NAME = "NAME";
    public static final String LAST_NAME = "LNAME";
    public static final String HEIGHT = "HEIGHT";
    public static final String POUNDS = "POUNDS";
    public static final String AGE = "AGE";
    public static final String IMAGE = "IMAGE";


    public static final String CB_TABLE = "CheckBox_table";
    public static final String IDCB = "IDCB";
    public static final String CALENDAR = "CALENDAR";
    public static final String FB = "FACEBOOKROW";
    public static final String PH = "PHONEROW";
    public static final String INSTAGRAM = "INSTAGRAMROW";
    public static final String DATE = "DATE";
    public static final String KISS = "KISS";
    public static final String MO = "MAKE_OUT";
    public static final String SEX = "SEX";
    public static final String COMMENT = "COMMENT";
    public static final String BF_ID = "BF_ID";
    public static final String JEALOUSY = "JEALOUSY";
    public static final String CANCEL_DATE = "CANCEL_DATE";
    public static final String NAGGING = "NAGGING";
    public static final String INDIFFERENCE = "INDIFFERENCE";
    public static final String CHEATING = "CHEATING";
    public static final String NOT_TALK = "NOT_TALK";
    public static final String BREAKING = "BREAKING";
    public static final String DATECAL = "DATECAL";
    public static final String FBCHECK = "FaceBook";
    public static final String PHCHECK = "Phone";
    public static final String INSCHECK = "Instagram";


    public static final String fb = " ";

    public DataBase(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + BF_TABLE + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,NAME TEXT,LNAME TEXT,HEIGHT TEXT,POUNDS TEXT,AGE TEXT,IMAGE TEXT)");
        db.execSQL("create table " + CB_TABLE + "(" + IDCB + " INTEGER PRIMARY KEY AUTOINCREMENT,CALENDAR TEXT,FACEBOOKROW TEXT,PHONEROW TEXT,INSTAGRAMROW TEXT,DATE TEXT,KISS TEXT," +
                "MAKE_OUT TEXT,SEX TEXT,COMMENT TEXT,BF_ID TEXT,JEALOUSY TEXT,CANCEL_DATE TEXT,NAGGING TEXT,INDIFFERENCE TEXT,CHEATING TEXT,NOT_TALK TEXT,BREAKING TEXT,DATECAL LONG,FaceBook TEXT" +
                ",Phone TEXT,Instagram TEXT)");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE " + BF_TABLE);
        db.execSQL("DROP TABLE " + CB_TABLE);

    }

    public Boolean data(String name, String lName, String height, String pounds, String age, String image) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME, name);
        contentValues.put(LAST_NAME, lName);
        contentValues.put(HEIGHT, height);
        contentValues.put(POUNDS, pounds);
        contentValues.put(AGE, age);
        contentValues.put(IMAGE, image);

        long result = db.insert(BF_TABLE, null, contentValues);
        if (result == -1)
            return false;
        else return true;


    }

    public Boolean data2(String calendar, String fb, String ph, String inst, String date, String kiss, String makeOut, String sex, String comm, String bfid, String jeal, String cancel, String nagg,
                         String indiffe, String cheating, String talk, String breaking, long datecal, String cfb, String cph, String cins) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CALENDAR, calendar);
        contentValues.put(FB, fb);
        contentValues.put(PH, ph);
        contentValues.put(INSTAGRAM, inst);
        contentValues.put(DATE, date);
        contentValues.put(KISS, kiss);
        contentValues.put(MO, makeOut);
        contentValues.put(SEX, sex);
        contentValues.put(COMMENT, comm);
        contentValues.put(BF_ID, bfid);
        contentValues.put(JEALOUSY, jeal);
        contentValues.put(CANCEL_DATE, cancel);
        contentValues.put(NAGGING, nagg);
        contentValues.put(INDIFFERENCE, indiffe);
        contentValues.put(CHEATING, cheating);
        contentValues.put(NOT_TALK, talk);
        contentValues.put(BREAKING, breaking);
        contentValues.put(DATECAL, datecal);
        contentValues.put(FBCHECK, cfb);
        contentValues.put(PHCHECK, cph);
        contentValues.put(INSCHECK, cins);


        long result = db.insert(CB_TABLE, null, contentValues);
        if (result == -1)
            return false;
        else return true;


    }

    public Cursor getAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from " + BF_TABLE, null);
        return res;
    }

    public Cursor getAll2() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from " + CB_TABLE, null);
        return res;
    }


    public Integer delete(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(BF_TABLE, "ID = ?", new String[]{id});

    }


    public Integer deleteCk(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(CB_TABLE, "BF_ID = ?", new String[]{id});

    }

    public Integer deleteCkFron(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(CB_TABLE, "IDCB = ?", new String[]{id});

    }


    public Cursor innerJoin(String id) {
        SQLiteDatabase db = this.getWritableDatabase();

//
//        select * from (select * from Boyfriend_table inner join CheckBox_table on BF_ID = ID where BF_ID = 1
//                order by IDCB DESC limit 1) order by IDCB ASC;
//

        String query = "select * from " + "( select * from " + BF_TABLE + "  INNER JOIN  " + CB_TABLE + " ON " + BF_ID + " = " + "ID" + " WHERE " + BF_ID + " = " + id + " order by "
                + IDCB + " desc limit " + 5 + " )" + " order by " + IDCB + " ASC ";

        Cursor res = db.rawQuery(query, null);


        return res;


    }

    public Cursor select(String id, long from, long to) {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.d("ok", "mpaaaaa" + id + " " + from + " " + to);

        String query = "select * from " + BF_TABLE + "  INNER JOIN  " + CB_TABLE + " ON " + BF_ID + " = " + "ID" + " WHERE " + BF_ID + " = " + id +
                " and " + DATECAL + " between " + from + " and " + to;
        Cursor res = db.rawQuery(query, null);
        return res;
    }

    public boolean update(String id, String name, String lName, String height, String pounds, String age, String image) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME, name);
        contentValues.put(LAST_NAME, lName);
        contentValues.put(HEIGHT, height);
        contentValues.put(POUNDS, pounds);
        contentValues.put(AGE, age);
        contentValues.put(IMAGE, image);
        db.update(BF_TABLE, contentValues, "ID = ?", new String[]{id});
        return true;
    }

    public boolean updateCk(String id, String calendar, String fb, String ph, String inst, String date, String kiss, String mo, String sex, String comment, String bfid,
                            String jeal, String cancel, String nagg,
                            String indiffe, String cheating, String talk, String breaking) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CALENDAR, calendar);
        contentValues.put(FB, fb);
        contentValues.put(PH, ph);
        contentValues.put(INSTAGRAM, inst);
        contentValues.put(DATE, date);
        contentValues.put(KISS, kiss);
        contentValues.put(MO, mo);
        contentValues.put(SEX, sex);
        contentValues.put(COMMENT, comment);
        contentValues.put(BF_ID, bfid);
        contentValues.put(JEALOUSY, jeal);
        contentValues.put(CANCEL_DATE, cancel);
        contentValues.put(NAGGING, nagg);
        contentValues.put(INDIFFERENCE, indiffe);
        contentValues.put(CHEATING, cheating);
        contentValues.put(NOT_TALK, talk);
        contentValues.put(BREAKING, breaking);
        db.update(CB_TABLE, contentValues, "IDCB = ?", new String[]{id});
        return true;
    }


    public int getLastId() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT  * FROM " + BF_TABLE, null);

        int id = 0;
        if (cursor.moveToLast()) {

            id = cursor.getInt(0);//to get id, 0 is the column index


        }
        return id;
    }

    public Boolean chFace(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String ko = "kfodkfso";


        Cursor res = db.rawQuery("SELECT * from " + CB_TABLE + " where " + BF_ID + " = " + id + " and " + FB + " = " + FBCHECK, null);
        if (res.getCount() == 0) {
            return true;
        } else return false;

    }

    public Boolean chPhone(String id) {
        SQLiteDatabase db = this.getWritableDatabase();


        Cursor res = db.rawQuery("SELECT * from " + CB_TABLE + " where " + BF_ID + " = " + id + " and " + PH + " = " + PHCHECK, null);
        if (res.getCount() == 0) {
            return true;
        } else return false;

    }

    public Boolean chInsta(String id) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor res = db.rawQuery("SELECT * from " + CB_TABLE + " where " + BF_ID + " = " + id + " and " + INSTAGRAM + " = " + INSCHECK, null);
        if (res.getCount() == 0) {
            return true;
        } else return false;

    }

}