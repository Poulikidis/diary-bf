package com.calendartemplate;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.datamedic.boyfrienddiary.R;

import java.util.ArrayList;
import java.util.Calendar;

import static com.calendartemplate.FrontList.calendarIntFrom;
import static com.calendartemplate.FrontList.calendarIntTo;

public class SelectList extends MainActivity{

    DataBase dataBase;
    Button Back;
    ListView listView;
    CheckBoxList checkBoxList;



    Button select;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selectlist);
        dataBase = new DataBase(this);
        Cursor res = dataBase.select(getBfId(),FrontList.getCalendarIntFrom(),FrontList.getCalendarIntTo());


        listView = findViewById(R.id.ListSelect);




        final ArrayList<CheckBoxClass> theList = new ArrayList<>();
        if (res.getCount() == 0)
            Toast.makeText(SelectList.this, " Nothing Stored in Database", Toast.LENGTH_SHORT);
        else
            while (res.moveToNext()) {
                CheckBoxClass checkBoxClass = new CheckBoxClass(res.getString(1), res.getString(2)
                        , res.getString(8), res.getString(9)
                        , res.getString(10), res.getString(11), res.getString(12)
                        , res.getString(13), res.getString(14), res.getString(15), res.getString(16), res.getString(7), res.getString(18),
                        res.getString(19), res.getString(20), res.getString(21), res.getString(22), res.getString(23), res.getString(24));
                theList.add(checkBoxClass);

                checkBoxList = new CheckBoxList(this, R.layout.cklist, theList);
                listView.setAdapter(checkBoxList);

                calendarIntFrom = 0;
                calendarIntTo = 0;


            }


        Back = findViewById(R.id.BackFrontList);

        Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), FrontList.class);
                startActivity(intent);
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), FrontList.class);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }


}

